import { MonAn } from './model-v1.js';
import showInfo, { layThongTinTuForm } from './controller-v1.js';
// import { MonAn } from './model-v1.js';
let themMon = () => {
  let data = layThongTinTuForm();
  // tạo object từ class
  let { ten, ma, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  let monAn = new MonAn(
    ten,
    ma,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  showInfo(monAn);
};

document.getElementById('btnThemMon').addEventListener('click', themMon);
