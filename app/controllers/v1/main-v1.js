import { Dish } from '../../models/v1/model-v1.js';
import showInfor, { getInforFromForm } from './controller-v1.js';
// import showInfo from './controller-v1';

let addDish = () => {
  console.log('add');
  let data = getInforFromForm();
  console.log('data: ', data);
  // viết theo ES6
  // let dish = new Dish(data.ten)
  // array cần đúng thứ tự, object không cần đúng thứ tự chỉ cần đúng key
  let {
    foodID,
    foodName,
    foodType,
    foodPrice,
    promotion,
    status,
    foodPicture,
    describe,
  } = data;
  //   tạo object từ class

  let dish = new Dish(
    foodID,
    foodName,
    foodType,
    foodPrice,
    promotion,
    status,
    foodPicture,
    describe
  );
  console.log('dish: ', dish);
  showInfor(dish);
};
document.getElementById('btnThemMon').addEventListener('click', addDish);
