export let getInforFromForm = () => {
  let foodID = document.getElementById('foodID').value;
  let foodName = document.getElementById('tenMon').value;
  let foodType = document.getElementById('loai').value;
  let foodPrice = document.getElementById('giaMon').value;
  let promotion = document.getElementById('khuyenMai').value;
  let status = document.getElementById('tinhTrang').value;
  let foodPicture = document.getElementById('hinhMon').value;
  let describe = document.getElementById('moTa').value;
  return {
    foodID,
    foodName,
    foodType,
    foodPrice,
    promotion,
    status,
    foodPicture,
    describe,
  };
};

let showInfor = (food) => {
  document.getElementById('spMa').innerHTML = food.foodID;
  document.getElementById('spTenMon').innerHTML = food.foodName;
  document.getElementById('spLoaiMon').innerHTML = food.foodType;
  document.getElementById('spGia').innerHTML = currency(food.foodPrice, {
    symbol: 'đ',
  }).format();
  document.getElementById('spKM').innerHTML = food.promotion + '%';
  const promotionPrice = (food.foodPrice * (100 - food.promotion)) / 100;
  document.getElementById('spGiaKM').innerHTML = currency(promotionPrice, {
    symbol: 'đ',
  }).format();
  document.getElementById('spTT').innerHTML = food.status;
  document.getElementById('imgMonAn').innerHTML = food.foodPicture;
  document.getElementById('pMoTa').innerHTML = food.describe;
};

export default showInfor;
