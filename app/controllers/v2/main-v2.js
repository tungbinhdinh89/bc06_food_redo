// import js to main
import { renderFoodList } from './controller-v2.js';
import { getInforFromForm } from '../v1/controller-v1.js';
import { pushInforToForm } from './controller-v2.js';
import { showMessage } from './controller-v2.js';
import { onLoading } from './controller-v2.js';
import { offLoading } from './controller-v2.js';

// get data from axios and render food list
const BASE_URL = `https://64000df463e89b0913a662f5.mockapi.io`;
let fetchFoodList = () => {
  onLoading();
  axios({
    url: `${BASE_URL}/FoodList`,
    method: `get`,
  })
    .then((res) => {
      console.log(res);
      renderFoodList(res.data);
      //   console.log('renderFoodList: ', renderFoodList);
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
fetchFoodList();

// delete food
window.deleteFood = (foodID) => {
  onLoading();
  axios({
    url: `${BASE_URL}/FoodList/${foodID}`,
    method: `delete`,
  })
    .then((res) => {
      console.log(res);
      fetchFoodList();
      showMessage('delete food success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
// edit food

window.editFood = (foodID) => {
  onLoading();
  console.log('foodID: ', foodID);
  getInforFromForm();

  axios({
    url: `${BASE_URL}/FoodList/${foodID}`,
    method: `get`,
  })
    .then((res) => {
      console.log(res);
      pushInforToForm(res.data);
      console.log('getInforFromForm(): ', getInforFromForm());
      document.getElementById('exampleModalLabel').innerHTML = 'Sửa Món Ăn';
      $('#exampleModal').modal('show');
      document.getElementById('btnCapNhat').style.display = 'flex';
      document.getElementById('btnThemMon').style.visibility = 'hidden';
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

// update food
window.updateFood = () => {
  onLoading();

  let FoodList = getInforFromForm();
  axios({
    url: `${BASE_URL}/FoodList/${FoodList.foodID}`,
    method: 'put',
    data: FoodList,
  })
    .then((res) => {
      console.log(res);
      fetchFoodList();
      $('#exampleModal').modal('hide');
      showMessage('update food success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

// create food
window.createFood = () => {
  onLoading();

  axios({
    url: `${BASE_URL}/FoodList`,
    method: 'post',
    data: getInforFromForm(),
  })
    .then((res) => {
      console.log(res);
      fetchFoodList();
      $('#exampleModal').modal('hide');
      showMessage('create food success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

window.addFood = () => {
  document.getElementById('exampleModalLabel').innerHTML = 'Thêm Món Ăn';
  document.getElementById('btnCapNhat').style.display = 'none';
  document.getElementById('btnThemMon').style.visibility = 'visible';
};
