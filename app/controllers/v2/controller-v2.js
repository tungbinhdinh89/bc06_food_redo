// render food list
export let renderFoodList = (foodArr) => {
  let contentHTML = ``;
  foodArr.forEach((item) => {
    // console.log('item: ', item);
    let contentTr = `
    <tr>
    <td>${item.foodID}</td>
    <td>${item.foodName}</td>
    <td>${item.foodType ? 'Món Chay' : 'Món Mặn'}</td>
    <td>${currency(item.foodPrice, {
      symbol: 'đ',
    }).format()}</td>
    <td>${item.promotion + '%'}</td>
    <td>${currency((item.foodPrice * (100 - item.promotion)) / 100, {
      symbol: 'đ',
    }).format()}</td>
    <td>${item.status ? 'Còn' : 'Hết'}</td>
    <td class='btn btn-danger btn-sm p-2' onclick='deleteFood(${
      item.foodID
    })'>Xoá</td>
    <td></td>
    <td class='btn btn-warning btn-sm p-2' onclick='editFood(${
      item.foodID
    })'>Sửa</td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById('tbodyFood').innerHTML = contentHTML;
};

// push infor to form

export let pushInforToForm = (inforFoodList) => {
  // let foodType = inforFoodList.foodType ? 'Món Chay' : 'Món Mặn';
  // console.log('foodType: ', foodType);
  document.getElementById('foodID').value = inforFoodList.foodID;
  document.getElementById('tenMon').value = inforFoodList.foodName;
  document.getElementById('loai').value = inforFoodList.foodType
    ? 'loai1'
    : 'loai2';
  document.getElementById('giaMon').value = inforFoodList.foodPrice;
  document.getElementById('khuyenMai').value = inforFoodList.promotion;
  document.getElementById('tinhTrang').value = inforFoodList.status ? '1' : '0';
  document.getElementById('hinhMon').value = inforFoodList.foodPicture;
  document.getElementById('moTa').value = inforFoodList.describe;
};

// on loading
export let onLoading = () => {
  document.querySelector('.loading').style.display = 'flex';
};
// off loading
export let offLoading = () => {
  document.querySelector('.loading').style.display = 'none';
};

// show message

export let showMessage = (mesage) => {
  Toastify({
    text: mesage,
    duration: 3000,
    destination: 'https://github.com/apvarun/toastify-js',
    newWindow: true,
    close: true,
    gravity: 'top', // `top` or `bottom`
    position: 'right', // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: 'linear-gradient(to right, #00b09b, #96c93d)',
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
