export class Dish {
  constructor(
    foodID,
    foodName,
    foodType,
    foodPrice,
    promotion,
    status,
    foodPicture,
    describe
  ) {
    (this.foodID = foodID),
      (this.foodName = foodName),
      (this.foodType = foodType),
      (this.foodPrice = foodPrice),
      (this.promotion = promotion),
      (this.status = status),
      (this.foodPicture = foodPicture),
      (this.describe = describe);
  }
}
