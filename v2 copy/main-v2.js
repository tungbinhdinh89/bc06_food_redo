import { onSuccess, renderFoodList } from './controller-v2.js';

import { layThongTinTuForm } from '../v1/controller-v1.js';

// true => chay, còn
// lấy dữ liệu từ server
let BASEURL = 'https://64000df463e89b0913a662f5.mockapi.io';
let fetchFoodList = () => {
  axios({
    url: `${BASEURL}/foods`,
    method: `get`,
  })
    .then((res) => {
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log('err: ', err);
    });
};
fetchFoodList();

let deleteFood = (id) => {
  axios({
    url: `${BASEURL}/foods/${id}`,
    method: 'delete',
  })
    .then((res) => {
      console.log('res: ', res);
      onSuccess('Xoá thành công');
      fetchFoodList();
    })
    .catch((err) => {
      console.log('err: ', err);
    });
};
window.deleteFood = deleteFood;

let createFood = () => {
  let data = layThongTinTuForm();
  console.log('data: ', data);
  axios({
    url: `${BASEURL}/foods`,
    method: `post`,
    // data: data
    data,
  })
    .then((res) => {
      console.log(res);
      // đọc từ khoá hide modal bs4
      $('#exampleModal').modal('hide');
      onSuccess('thêm thành công');
    })
    .catch((err) => {
      console.log(err);
    });
};

window.createFood = createFood;

window.editFood = () => {
  console.log('yes');
  $('#exampleModal').modal('show');
};
