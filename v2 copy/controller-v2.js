export let renderFoodList = (foodArr) => {
  let contentHTML = '';
  foodArr.forEach((item) => {
    let contentTr = `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.type ? 'chay' : 'mặn'} </td>
    <td>${item.price}</td>
    <td>${item.discount}</td>
    <td>0</td>
    <td>${item.status ? 'còn' : 'hết'} </td>
    <td><button class='btn btn-danger' onclick='deleteFood(${
      item.id
    })'> Xoá</button></td>
    <td><button class='btn btn-warning' onclick='editFood(${
      item.id
    })'> sửa</button></td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById('tbodyFood').innerHTML = contentHTML;
};
export let onSuccess = (messeager) => {
  Toastify({
    text: messeager,
    duration: 3000,
    destination: 'https://github.com/apvarun/toastify-js',
    newWindow: true,
    close: true,
    gravity: 'top', // `top` or `bottom`
    position: 'right', // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: 'linear-gradient(to right, #00b09b, #96c93d)',
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
